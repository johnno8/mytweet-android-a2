# MyTweet

MyTweet is a microblogging app for the Android OS. It was developed incrementally in Android Studio with improved functionality added over time. It allows a user to register, log in, post and delete tweets, view a list of their own tweets in a timeline and email the contents of a tweet to an address from their contact list.

### Quick Start
The app consumes an API provided by a web app called [MyTweet](https://bitbucket.org/johnno8/mytweet-web-a2/overview) and is configured to post new user and new tweet data to the web app, and to pull down registered users and tweets at startup. It refreshes this data periodically through a Broadcast Refresh service. Login is also carried out via the API in order to allow authentication via BCrypt in the web app, which is not available for Android.
##### Valid Demo Login
      USER
        email: homer@simpson.com
        password: secret



### Known Bugs

  - The createTweet API call successfully creates a Tweet which can be seen in the DB and is displayed in the Timeline after a refresh, but the onResponse method of the Retrofit call is never hit - it fails for a JSONIllegalStateException
  - The Clear menu button in the Timeline clears local storage only - deletion of other User's tweets is forbidden, so as soon as the cache is refreshed the tweets will be redisplayed
  - Login is inherently insecure because login details are passed in the clear through an API call to the web app. This is necessary since passwords are hashed and salted with BCrypt in the web app, and Android does not support BCrypt, login without the API call would fail