package org.wit.mytweet.models;

import java.util.ArrayList;
import java.util.List;

import static org.wit.mytweet.android.helpers.LogHelpers.info;
import android.content.Context;
import android.util.Log;

import org.wit.mytweet.sqlite.DbHelper;

/**
 * Created by John on 10/10/2016.
 */
public class Tweetlist
{
    public ArrayList<Tweet> tweets;
    //private TweetlistSerializer serializer;
    public DbHelper dbHelper;

    /*public Tweetlist()
    {
        tweets = new ArrayList<Tweet>();
    }*/

    public Tweetlist(Context context)
    {
        /*this.serializer = serializer;
        try
        {
            tweets = serializer.loadTweets();
        }
        catch (Exception e)
        {
            info(this, "Error loading tweets: " + e.getMessage());
            tweets = new ArrayList<Tweet>();
        }*/
        try {
            dbHelper = new DbHelper(context);
            tweets = (ArrayList<Tweet>) dbHelper.selectTweets();
        }
        catch (Exception e) {
            info(this, "Error loading tweets: " + e.getMessage());
            tweets = new ArrayList<Tweet>();
        }
    }

    /**
     * Obtain the entire database of tweets
     *
     * @return All the tweets in the database as an ArrayList
     */
    public ArrayList<Tweet> selectTweets() {
        return (ArrayList<Tweet>) dbHelper.selectTweets();
    }

    /**
     * Add incoming tweet to both local and database storage
     *
     * @param tweet The tweet object to be added to local and database storage.
     */
    public void addTweet(Tweet tweet) {
        tweets.add(tweet);
        dbHelper.addTweet(tweet);
    }

    /**
     * Obtain specified tweet from local list and return.
     *
     * @param id The String _id identifier of the tweet sought.
     * @return The specified tweet if it exists.
     */
    public Tweet getTweet(String id) {
        Log.i(this.getClass().getSimpleName(), "String _id _id: " + id);

        for (Tweet t : tweets) {
            if (id.equals(t._id)) {
                return t;
            }
        }
        info(this, "failed to find tweet. returning first element array to avoid crash");
        return null;
    }

    /**
     * Delete Tweet object from local and remote storage
     *
     * @param tweet Tweet object for deletion.
     */
    public void deleteTweet(Tweet tweet) {
        dbHelper.deleteTweet(tweet);
        tweets.remove(tweet);
    }

    public void updateTweet(Tweet tweet) {
        dbHelper.updateTweet(tweet);
        updateLocalTweets(tweet);

    }

    /**
     * Clear local and sqlite tweets and refresh with incoming list.
     * @param tweets List tweet objects
     */
    public void refreshTweets(List<Tweet> tweets)
    {
        dbHelper.deleteTweets();
        this.tweets.clear();

        dbHelper.addTweets(tweets);

        for (int i = 0; i < tweets.size(); i += 1) {
            this.tweets.add(tweets.get(i));
        }
    }

    /**
     * Search the list of tweets for argument tweet
     * If found replace it with argument tweet.
     * If not found just add the argument tweet.
     *
     * @param tweet The Tweet object with which the list of Tweets to be updated.
     */
    private void updateLocalTweets(Tweet tweet) {
        for (int i = 0; i < tweets.size(); i += 1) {
            Tweet t = tweets.get(i);
            if (t._id.equals(tweet._id)) {
                tweets.remove(i);
                tweets.add(tweet);
                return;
            }
        }
    }

    public int getSize()
    {
        return tweets.size();
    }

    public void clearTimeline()
    {
        tweets.clear();
    }

    /*
    public boolean saveTweets()
    {
        try
        {
            serializer.saveTweets(tweets);
            info(this, "Tweets saved to file");
            return true;
        }
        catch (Exception e)
        {
            info(this, "Error saving tweets: " + e.getMessage());
            return false;
        }
    }

    public void newTweet(Tweet tweet)
    {
        tweets.add(tweet);
    }

    public Tweet getTweet(Long _id)
    {
        for(Tweet t : tweets)
        {
            if(_id.equals(t._id))
            {
                return t;
            }
        }
        return null;
    }

    public int getSize()
    {
        return tweets.size();
    }

    public void clearTimeline()
    {
        tweets.clear();
    }

    public void deleteTweet(Tweet tweet)
    {
        tweets.remove(tweet);
        saveTweets();
    }*/
}
