package org.wit.mytweet.models;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 * Created by John on 07/10/2016.
 */
public class Tweet
{
    public String _id;
    public String content;
    public User tweetor;
    public String date;

    private static final String JSON_ID = "_id";
    private static final String JSON_TWEETEXT = "content";
    private static final String JSON_TWEETGENERATIONDATE = "date";


    public Tweet(String content)
    {
        this.content = content;
        this.date = new SimpleDateFormat("dd-MM-yyyy H:mm").format(new Date());
    }

    public Tweet()
    {
        this._id = null;
        this.content = null;
        this.tweetor = null;
        this.date = null;
    }

    public String getContent()
    {
        return content;
    }

    /**
     * Generate a number string greater than zero
     * @return number string value greater than zero
     */
    private String generateId() {
        long rnd = 0;
        String rndVal;
        do {
            rnd = new Random().nextLong();
            rndVal = String.valueOf(rnd);
        } while (rnd <= 0);
        return rndVal;
    }

    public Tweet(JSONObject json) throws JSONException
    {
        _id = json.getString(JSON_ID);
        content = json.getString(JSON_TWEETEXT);
        date = json.getString(JSON_TWEETGENERATIONDATE);
    }

    public JSONObject toJSON() throws JSONException
    {
        JSONObject json = new JSONObject();
        json.put(JSON_ID            , _id);
        json.put(JSON_TWEETEXT   , content);
        json.put(JSON_TWEETGENERATIONDATE          , date);
        return json;
    }
}
