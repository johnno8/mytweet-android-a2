package org.wit.mytweet.app;

import org.wit.mytweet.models.Tweet;
import org.wit.mytweet.models.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by John on 22/12/2016.
 */
public interface  MyTweetService
{
    @GET("/api/users")
    Call<List<User>> getAllUsers();

    @GET("/api/users/{id}")
    Call<User> getUser(@Path("id") String id);

    @POST("/api/users")
    Call<User> createUser(@Body User User);

    @GET("/api/users/{email}/{password}")
    Call<String> login(@Path("email") String email, @Path("password") String password);

    @GET("/api/tweets")
    Call<List<Tweet>> getAllTweets();

    @GET("/api/users/{id}/tweets")
    Call<List<Tweet>> getUsersTweets(@Path("id") String id);

    @POST("/api/users/{id}/tweets")
    Call<Tweet> sendTweet(@Path("id") String id, @Body Tweet tweet);

    @DELETE("/api/tweets/{id}")
    Call<String> deleteTweet(@Path("id") String id);
}
