package org.wit.mytweet.app;

import android.app.Application;
import android.content.Intent;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.wit.mytweet.models.Tweet;
import org.wit.mytweet.models.Tweetlist;
import org.wit.mytweet.models.User;
import org.wit.mytweet.sqlite.DbHelper;

import static org.wit.mytweet.android.helpers.LogHelpers.info;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by John on 05/10/2016.
 */
public class MyTweetApp extends Application
{
    public MyTweetService mytweetService;
    public boolean mytweetServiceAvailable = false;

    //public String service_url  = "http://10.0.2.2:4000";   // Standard Emulator IP Address
    public String service_url  = "https://boiling-lake-56131.herokuapp.com";

    public User currentUser;

    public List<User> users = new ArrayList<User>();
    public List<Tweet> tweets = new ArrayList<Tweet>();
    public DbHelper dbHelper;

    protected static MyTweetApp app;

    @Override
    public void onCreate()
    {
        super.onCreate();
        super.onCreate();
        Gson gson = new GsonBuilder().create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(service_url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        mytweetService = retrofit.create(MyTweetService.class);

        dbHelper = new DbHelper(getApplicationContext());

        info(this, "MyTweet app launched");
        sendBroadcast(new Intent("org.wit.mytweet.receivers.SEND_BROADCAST"));
    }

    /**
     * Add a new user to local ArrayList of users
     * @param user new user object to be added to list
     */
    public void newUser(User user)
    {
        users.add(user);
    }

    /**
     * Add a new tweet to local ArrayList of tweets and to local db SQLite
     * @param tweet
     */
    public void newTweet(Tweet tweet)
    {
        dbHelper.addTweet(tweet);
        tweets.add(tweet);
    }

    /**
     * Remove tweet from local ArrayList and SQLite
     * @param tweet
     */
    public void deleteTweet(Tweet tweet)
    {
        dbHelper.deleteTweet(tweet);
        tweets.remove(tweet);
    }

    /**
     * Clear local and sqlite tweets and refresh with incoming list.
     * @param tweets List Tweet objects
     */
    public void refreshTweets(List<Tweet> tweets)
    {
        dbHelper.deleteTweets();
        this.tweets.clear();

        dbHelper.addTweets(tweets);

        for (int i = 0; i < tweets.size(); i += 1) {
            this.tweets.add(tweets.get(i));
        }
    }

    /**
     * Compares supplied login details to list of registered users; if a match is found, currentUser
     * is set to that user
     * @param email email address from user for validation
     * @param password password from user for validation
     * @return
     */
    public boolean validUser(String email, String password)
    {
        for(User user : users)
        {
            if(user.email.equals(email) && user.password.equals(password))
            {
                currentUser = user;
                return true;
            }
        }
        return false;
    }

    public static MyTweetApp getApp(){
        return app;
    }

    /**
     * Searches for a tweet based on the id supplied, if found tweet object is returned
     * @param id supplied tweet id
     * @return
     */
    public Tweet getTweet(String id)
    {
        for(Tweet tweet : tweets)
        {
            if(tweet._id.equals(id))
            {
                return tweet;
            }
        }
        return null;
    }

}
