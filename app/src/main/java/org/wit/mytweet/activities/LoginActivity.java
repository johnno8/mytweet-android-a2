package org.wit.mytweet.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.wit.mytweet.R;
import org.wit.mytweet.app.MyTweetApp;
import org.wit.mytweet.models.Tweet;
import org.wit.mytweet.models.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements Callback<String>
{
    private MyTweetApp app;
    private TextView email;
    private String emailStr;
    private TextView password;
    private String passwordStr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    public void loginApplyButtonPressed(View view)
    {
        app = (MyTweetApp) getApplication();

        email = (TextView) findViewById(R.id.loginEmail);
        password = (TextView) findViewById(R.id.loginPassword);
        emailStr = email.getText().toString();
        passwordStr = password.getText().toString();

        Call<String> call = app.mytweetService.login(emailStr, passwordStr);
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<String> call, Response<String> response) {
        Toast.makeText(this, "Login successful", Toast.LENGTH_SHORT).show();
        for(User user : app.users)
        {
            if(user.email.equals(emailStr))
            {
                app.currentUser = user;
            }
        }
        startActivity(new Intent(this, TimelineActivity.class));
    }

    @Override
    public void onFailure(Call<String> call, Throwable t) {
        Toast.makeText(this, "Login failed", Toast.LENGTH_SHORT).show();
    }


}
