package org.wit.mytweet.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import org.wit.mytweet.R;
import org.wit.mytweet.app.MyTweetApp;
import org.wit.mytweet.models.Tweet;
import org.wit.mytweet.models.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WelcomeActivity extends AppCompatActivity implements Callback<List<User>>
{
    private MyTweetApp app;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        app = (MyTweetApp) getApplication();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        app.currentUser = null;

        //api call to return a list of all registered users
        Call<List<User>> call1 = (Call<List<User>>) app.mytweetService.getAllUsers();
        call1.enqueue(this);

        //api call to return list of all tweets using anonymous inner function
        Call<List<Tweet>> call2 = (Call<List<Tweet>>) app.mytweetService.getAllTweets();
        call2.enqueue(new Callback<List<Tweet>>() {
            @Override
            public void onResponse(Call<List<Tweet>> call, Response<List<Tweet>> response) {
                app.tweets = response.body();
            }

            @Override
            public void onFailure(Call<List<Tweet>> call, Throwable t) {
                app.mytweetServiceAvailable = false;
                serviceUnavailableMessage();
            }
        });

    }


    public void signupPressed(View view)
    {
        if (app.mytweetServiceAvailable)
        {
            startActivity (new Intent(this, SignupActivity.class));
        }
        else
        {
            serviceUnavailableMessage();
        }
    }

    public void loginPressed(View view)
    {
        if (app.mytweetServiceAvailable)
        {
            startActivity (new Intent(this, LoginActivity.class));
        }
        else
        {
            serviceUnavailableMessage();
        }
    }

    @Override
    public void onResponse(Call<List<User>> call, Response<List<User>> response)
    {
        serviceAvailableMessage();
        app.users = response.body();
        app.mytweetServiceAvailable = true;
    }

    @Override
    public void onFailure(Call<List<User>> call, Throwable t)
    {
        app.mytweetServiceAvailable = false;
        serviceUnavailableMessage();
    }


    void serviceUnavailableMessage()
    {
        Toast toast = Toast.makeText(this, "MyTweet Service Unavailable. Try again later", Toast.LENGTH_LONG);
        toast.show();
    }
    void serviceAvailableMessage()
    {
        Toast toast = Toast.makeText(this, "MyTweet Contacted Successfully", Toast.LENGTH_LONG);
        toast.show();
    }

}
