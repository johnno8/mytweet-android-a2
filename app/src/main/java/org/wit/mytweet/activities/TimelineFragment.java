package org.wit.mytweet.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AbsListView;
import android.view.ActionMode;
import android.widget.Toast;

import org.wit.mytweet.R;

import org.wit.mytweet.android.helpers.IntentHelper;
import org.wit.mytweet.app.MyTweetApp;
import org.wit.mytweet.models.Tweet;
import org.wit.mytweet.models.Tweetlist;
import org.wit.mytweet.settings.SettingsActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by John on 24/10/2016.
 */
public class TimelineFragment extends ListFragment implements AdapterView.OnItemClickListener,
        AbsListView.MultiChoiceModeListener, Callback<List<Tweet>>
{
    private ListView listView;
    private TweetAdapter adapter;
    MyTweetApp app;
    public List<Tweet> tweets;
    public static final String BROADCAST_ACTION = "org.wit.mytweet.activities.TimelineFragment";
    private IntentFilter intentFilter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        getActivity().setTitle(R.string.app_name);

        app = (MyTweetApp) getActivity().getApplication();
        tweets = app.tweets;

        adapter = new TweetAdapter(getActivity(), tweets);
        setListAdapter(adapter);

        registerBroadcastReceiver();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, parent, savedInstanceState);
        listView = (ListView) v.findViewById(android.R.id.list);
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        listView.setMultiChoiceModeListener(this);
        return v;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Tweet tweet = ((TweetAdapter) getListAdapter()).getItem(position);
        Intent i = new Intent(getActivity(), TweetActivity.class);
        i.putExtra("TWEET_ID", tweet._id);
        startActivityForResult(i, 0);
    }

    @Override
    public void onResume() {
        super.onResume();
        //((TweetAdapter) getListAdapter()).notifyDataSetChanged();

        //When Timeline is reopened (e.g. after posting a tweet) api call
        //updates the local list of tweets to match remote list
        //Call<List<Tweet>> call = (Call<List<Tweet>>) app.mytweetService.getAllTweets();
        //call.enqueue(this);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.timeline, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.menu_item_new_tweet: startActivity(new Intent(getActivity(), TweetActivity.class));
                return  true;

            case R.id.action_clear:
                app = MyTweetApp.getApp();
                tweets = app.tweets;
                if(tweets.size() > 0)
                {
                    tweets.clear();
                }
                startActivity(new Intent(getActivity(), TimelineActivity.class));
                return true;

            case R.id.action_settings:
                startActivity(new Intent(getActivity(), SettingsActivity.class));
                return true;

            case R.id.action_refresh:
                retrieveTweets();
                return true;

            default: return  super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Tweet tweet = adapter.getItem(position);
        IntentHelper.startActivityWithData(getActivity(), TweetActivity.class, "TWEET_ID", tweet._id);
    }

    private void registerBroadcastReceiver()
    {
        intentFilter = new IntentFilter(BROADCAST_ACTION);
        ResponseReceiver responseReceiver = new ResponseReceiver();
        // Registers the ResponseReceiver and its intent filters
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(responseReceiver, intentFilter);
    }

    /* ************ MultiChoiceModeListener methods (begin) *********** */
    @Override
    public boolean onCreateActionMode(ActionMode actionMode, Menu menu)
    {
        MenuInflater inflater = actionMode.getMenuInflater();
        inflater.inflate(R.menu.timeline_context, menu);
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode actionMode, Menu menu)
    {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem)
    {
        switch (menuItem.getItemId())
        {
            case R.id.menu_item_delete_tweet:
                deleteTweet(actionMode);
                return true;
            default:
                return false;
        }

    }

    private void deleteTweet(ActionMode actionMode)
    {
        for (int i = adapter.getCount() - 1; i >= 0; i--)
        {
            if (listView.isItemChecked(i))
            {
                Tweet tweet = adapter.getItem(i);

                if(tweet.tweetor._id.equals(app.currentUser._id))//disallows deletion of other user's tweet
                {
                    app.tweets.remove(tweet);
                    deleteTweet(tweet._id);
                }
                else
                {
                    Toast.makeText(getActivity(), "User cannot delete another user's tweets ", Toast.LENGTH_SHORT).show();
                }
            }
        }
        actionMode.finish();
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onDestroyActionMode(ActionMode actionMode)
    {
    }

    @Override
    public void onItemCheckedStateChanged(ActionMode actionMode, int position, long id, boolean checked)
    {
    }

    @Override
    public void onResponse(Call<List<Tweet>> call, Response<List<Tweet>> response) {
       /* List<Tweet> listTweets = response.body();
        Toast.makeText(getActivity(), "Retrieved " + listTweets.size() + " tweets", Toast.LENGTH_SHORT).show();
        app.refreshTweets(listTweets);
        ((TweetAdapter) getListAdapter()).notifyDataSetChanged();*/
    }

    @Override
    public void onFailure(Call<List<Tweet>> call, Throwable t) {
        //Toast.makeText(getActivity(), "Failed to retrieve tweet list", Toast.LENGTH_SHORT).show();
    }

    /* ************ MultiChoiceModeListener methods (end) *********** */


    /* ************ Retrofit: Delete One Tweet ************ */

    public void deleteTweet(String id)
    {
        DeleteRemoteTweet delTweet = new DeleteRemoteTweet();
        Call<String> call = app.mytweetService.deleteTweet(id);
        call.enqueue(delTweet);
    }

    class DeleteRemoteTweet implements Callback<String>
    {

        @Override
        public void onResponse(Call<String> call, Response<String> response) {
            Toast.makeText(getActivity(), "Tweet deleted", Toast.LENGTH_SHORT).show();
            adapter.notifyDataSetChanged();
            Log.v("onResponse ","Response: " + response.toString());
        }

        @Override
        public void onFailure(Call<String> call, Throwable t) {
            Toast.makeText(getActivity(), "Failed to delete Tweet", Toast.LENGTH_SHORT).show();
            Log.e("onFailure ", "Throwable: ", t);
        }
    }

    /* ************ Retrofit: Delete One Tweet (end)********* */


    /* ************ Retrofit: Refresh Tweets **************** */
    /*        implements api call for manual refresh          */

    public  void retrieveTweets()
    {
        RetrieveTweets retrieveTweets = new RetrieveTweets();
        Call<List<Tweet>> call = (Call<List<Tweet>>) app.mytweetService.getAllTweets();
        call.enqueue(retrieveTweets);
    }

    class RetrieveTweets implements Callback<List<Tweet>>
    {
        @Override
        public void onResponse(Call<List<Tweet>> call, Response<List<Tweet>> response) {
            List<Tweet> listTweets = response.body();
            Toast.makeText(getActivity(), "Retrieved " + listTweets.size() + " tweets", Toast.LENGTH_SHORT).show();
            app.refreshTweets(listTweets);
            ((TweetAdapter) getListAdapter()).notifyDataSetChanged();
        }

        @Override
        public void onFailure(Call<List<Tweet>> call, Throwable t) {
            Toast.makeText(getActivity(), "Failed to retrieve tweet list", Toast.LENGTH_SHORT).show();
        }
    }

    /* ************ Retrofit: Refresh Tweets (end)******* */

    //Broadcast receiver for receiving status updates from the IntentService
    private class ResponseReceiver extends BroadcastReceiver
    {
        // Called when the BroadcastReceiver gets an Intent it's registered to receive
        @Override
        public void onReceive(Context context, Intent intent)
        {
            //refreshDonationList();
            adapter.tweets = app.tweets;
            adapter.notifyDataSetChanged();
        }
    }

}

class TweetAdapter extends ArrayAdapter<Tweet>
{
    private Context context;
    List<Tweet> tweets;

    public TweetAdapter(Context context, List<Tweet> tweets)
    {
        super(context, 0, tweets);
        this.context = context;
        this.tweets = tweets;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
        {
            convertView = inflater.inflate(R.layout.list_item_tweet, null);
        }
        Tweet tweet = getItem(position);

        TextView tweetText = (TextView) convertView.findViewById(R.id.tweet_list_item_text);
        tweetText.setText(tweet.content);

        TextView tweetGenerationDate = (TextView) convertView.findViewById(R.id.tweet_list_item_date);
        tweetGenerationDate.setText(tweet.date);

        return convertView;
    }
}
