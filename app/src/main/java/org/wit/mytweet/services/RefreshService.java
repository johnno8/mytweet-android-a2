package org.wit.mytweet.services;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import org.wit.mytweet.activities.TimelineFragment;
import org.wit.mytweet.app.MyTweetApp;
import org.wit.mytweet.models.Tweet;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by John on 06/01/2017.
 */
public class RefreshService extends IntentService
{
    private String tag = "MyTweet";
    MyTweetApp app;
    public RefreshService()
    {
        super("RefreshService");
        //app = MyTweetApp.getApp();
        app = (MyTweetApp) getApplication();
    }

    @Override
    protected void onHandleIntent(Intent intent)
    {
        app = (MyTweetApp) getApplication();
        Intent localIntent = new Intent(TimelineFragment.BROADCAST_ACTION);
        Call<List<Tweet>> call = (Call<List<Tweet>>) app.mytweetService.getAllTweets();
        try
        {
            Response<List<Tweet>> response = call.execute();
            app.refreshTweets(response.body());
            LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);
        }
        catch (IOException e)
        {

        }
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        Log.i(tag, "RefreshService instance destroyed");
    }
}
